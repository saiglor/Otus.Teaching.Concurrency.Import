using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void AddCustomers(List<Customer> customers);

        Customer GetCustomer(int id);
    }
}