﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Bogus;
using Microsoft.AspNetCore.WebUtilities;

namespace Otus.Teaching.Concurrency.Import.App
{
    internal class Program
    {
        private static readonly HttpClient _client = new();

        static async Task Main(string[] args)
        {
            var randomCustomer = CreateFaker().Generate();

            await CreateUser(randomCustomer);
            var customer = await GetUser(50);

            Console.WriteLine(GetCustomerString(customer));
        }

        private static Faker<Customer> CreateFaker()
        {
            var customersFaker = new Faker<Customer>()
                .CustomInstantiator(f => new Customer()
                {
                    Id = new Random().Next(100, 200)
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }

        private static async Task CreateUser(Customer customer)
        {
            var json = JsonSerializer.Serialize(customer);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var request = await _client.PostAsync("https://localhost:44324/api/users/createuser", content);

            Console.WriteLine(request);
        }

        private static async Task<Customer> GetUser(int id)
        {
            var query = new Dictionary<string, string>
            {
                ["id"] = id.ToString()
            };

            var response = await _client.GetAsync(QueryHelpers.AddQueryString("https://localhost:44324/api/users/getuser", query));

            var json = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Customer>(json);
        }

        private static string GetCustomerString(Customer customer) =>
            $"{customer.Id} {customer.FullName} {customer.Email} {customer.Phone}";
    }
}
