using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.Concurrency.Import.WebApi
{
    public class Program
    {
        private static readonly string DataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            Console.WriteLine($"WebApi started with process Id {Environment.ProcessId}...");

            new ImportAppContextInit().DatabaseInit();

            GenerateCustomersDataFile();

            var loader = host.Services.GetService<IDataLoader>();

            loader?.LoadData(DataFilePath);

            host.Run();
        }

        private static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(DataFilePath, 100);
            xmlGenerator.Generate();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
