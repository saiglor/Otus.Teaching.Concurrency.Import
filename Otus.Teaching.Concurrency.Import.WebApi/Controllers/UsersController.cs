﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly ICustomerRepository _customerRepository;

        public UsersController()
        {
            _customerRepository = new CustomerRepository();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUser([FromQuery] int id)
        {
            var customer = _customerRepository.GetCustomer(id);

            if (customer == null) return NotFound();

            return Ok(customer);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> CreateUser([FromBody] Customer customer)
        {
            var isUserExist = _customerRepository.GetCustomer(customer.Id) is not null;
            if (isUserExist) { return Conflict(customer.Id); }

            _customerRepository.AddCustomer(customer);

            return Ok(customer.Id);
        }
    }
}