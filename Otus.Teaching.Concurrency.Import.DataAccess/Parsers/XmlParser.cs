﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public List<Customer> ParseFromFile(string path)
        {
            using var reader = new StreamReader(path);
            var deserializer = new XmlSerializer(typeof(List<Customer>),
                new XmlRootAttribute("Customers"));
            var customers = (List<Customer>)deserializer.Deserialize(reader);

            return customers;

        }
    }
}