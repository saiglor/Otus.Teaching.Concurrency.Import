using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly ImportAppContext _context;

        public CustomerRepository()
        {
            _context = new ImportAppContext();
        }

        public void AddCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
            _context.SaveChanges();
        }

        public void AddCustomers(List<Customer> customers)
        {
            foreach (var customer in customers)
            {
                _context.Customers.Add(customer);
            }
            _context.SaveChanges();
        }

        public Customer GetCustomer(int id)
        {
            return _context.Customers.Find(id);
        }
    }
}