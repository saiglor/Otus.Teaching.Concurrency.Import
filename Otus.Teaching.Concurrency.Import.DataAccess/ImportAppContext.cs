﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public sealed class ImportAppContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        
        public ImportAppContext()
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source=import.db");
    }
}