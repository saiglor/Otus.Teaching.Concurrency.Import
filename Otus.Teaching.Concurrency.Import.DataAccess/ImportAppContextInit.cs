﻿namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class ImportAppContextInit
    {
        private readonly ImportAppContext _context;

        public ImportAppContextInit()
        {
            _context = new ImportAppContext();
        }

        public void DatabaseInit()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
        }
    }
}