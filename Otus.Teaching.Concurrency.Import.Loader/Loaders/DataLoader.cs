using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader
        : IDataLoader
    {
        private const int CountThread = 16;
        private CountdownEvent _event;

        private readonly IDataParser<List<Customer>> _parser;

        public DataLoader(IDataParser<List<Customer>> parser)
        {
            _parser = parser;
        }

        public void LoadData(string path)
        {
            var customers = _parser.ParseFromFile(path);

            var stopwatch = Stopwatch.StartNew();

            _event = new CountdownEvent(CountThread);

            for (var i = 0; i < CountThread; i++)
            {
                var state = customers.Where((_, index) => index % CountThread == i).ToList();
                ThreadPool.QueueUserWorkItem(AddCustomers, state);
            }

            _event.Wait();

            stopwatch.Stop();

            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }

        private void AddCustomers(object obj)
        {
            var customers = (List<Customer>) obj;

            var customerRepository = new CustomerRepository();

            customerRepository.AddCustomers(customers);

            _event.Signal();
        }
    }
}