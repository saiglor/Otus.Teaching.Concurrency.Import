﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        
        static void Main(string[] args)
        {
            new ImportAppContextInit().DatabaseInit();

            var serviceProvider = new ServiceCollection()
                .AddSingleton<IDataParser<List<Customer>>, XmlParser>()
                .AddSingleton<IDataLoader, DataLoader>()
                .BuildServiceProvider();

            if (args is {Length: 1})
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Environment.ProcessId}...");

            GenerateCustomersDataFile();

            var loader = serviceProvider.GetService<IDataLoader>();
            
            loader?.LoadData(_dataFilePath);
        }

        private static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000000);
            xmlGenerator.Generate();
        }
    }
}